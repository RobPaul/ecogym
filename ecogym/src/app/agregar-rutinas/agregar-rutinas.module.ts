import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AgregarRutinasPage } from './agregar-rutinas.page';

const routes: Routes = [
  {
    path: '',
    component: AgregarRutinasPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AgregarRutinasPage]
})
export class AgregarRutinasPageModule {}
