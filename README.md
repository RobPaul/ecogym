# Ecogym

### Que es Ecogym?

Es un proyecto universitario impulsado por el grupo de programacion,
siendo esto un sistema completo, sin embargo esta parte del proyecto solo se enfoca
en el desarrollo movil.

---
### Cual es el target de Ecogym?

Todos los usuarios del gimnasio de Ecotec, permitiendoles acceder de manera eficiente a rutinas
necesarias para cada uno, siendo estas completamente personalizadas.

---
### Cual es el beneficio del grupo de programacion?

Netamente la experiencia y aprendizaje.

---
### Que los motivo a crear este proyecto?

El animo por programar y aprender mas alla de lo que la universidad podia ofrecer.

---

### Metodologias de desarrollo a utilizar

Se toma en cuenta el tiempo de desarrollo y curva de aprendizaje, por lo cual se establece que la metodologia
[SCRUM](https://www.sinnaps.com/blog-gestion-proyectos/metodologia-scrum), [XP](http://www.diegocalvo.es/metodologia-xp-programacion-extrema-metodologia-agil/) 
y [Componentes](http://marich.blogspot.es/1459441356/metodologia-basada-en-componentes/), son las adecuadas para el proyecto.

## Como instalar Ecogym?

Para instalar ecogym en un ambiente de desarrollo/produccion de software, debes descargar un [GUI manejador de Git](https://www.gitkraken.com/)
o abrir la terminal y utilizar directamente [los comandos necesarios](https://git-scm.com/doc) para el uso de GIT.

---
### Pasos para instalar el proyecto de ECOGYM

1. Clonar el repositorio actual
    1. Se puede realizar directamente con la URL en HTTPS o con [SSH](https://git-scm.com/book/es/v1/Git-en-un-servidor-Generando-tu-clave-p%C3%BAblica-SSH).
2. Abrir la terminal
3. Acceder a la carpeta donde el proyecto fue clonado.
4. Correr el comando **npm install** dentro del proyecto
    1. Se realiza este paso para la instalacion de las dependencias necesarias para el proyecto.

---

## Fechas de Commit y Push

Las fechas establecidas para subir cambios realizados en el codigo, seran los **LUNES DE CADA SEMANA** entre las **11:00pm - 11:59pm**.

---

## Reunion Previa a Subidas al repositorio
Previo a subir un cambio, se muestra en un reunion los cambios realizados y como estos aportan al avance del proyecto.

---
## Acceso al repositorio

En caso de no tener posiblidad de escribir o realizar cambios, consultar al administrador principal del proyecto, cual es su [rol](https://about.gitlab.com/handbook/product/#managing-creation-of-new-groups-stages-and-categories-1) perteneciente.

# PROHIBIDO SUBIR CAMBIOS DIRECTAMENTE A MASTER!
Cualquier cambio directo a la ramificacion **MASTER** sera completamente **borrado** o **revertido**

---
# Acerca de GitLab y Git
## Proceso de peticion de Merge
Las peticiones merge son publicaciones que se piden sean ya integradas en produccion, estas deben ser previamente aprobadas, enviando una solicitud
de aprobacion, una vez aprobada el cambio sera publicado en la rama **master**.

---
## Anotaciones y resumenes
En cada cambio realizado se pide un resumen y descripcion
1. Anotacion + Resumen :
    1. La anotacion se realiza segun la actividad de lo que se quiere subir, por ej: CREAR: home.
    2. Las anotaciones se dividen en: CREAR, ACTUALIZAR, REMOVER, ARREGLAR
    3. Las anotaciones deben ir seguidas  de dos puntos (:)
    4. Despues de los dos puntos, colocar el lugar donde fue realizado el cambio, actualizacion o creacion, por ej: ARREGLAR: Modulo Comida.
2. Descripcion:
    1. Las descripciones deben ser coherentes.
    2. Deben resumir la tarea que se esta realizando.

### NOTA IMPORTANTE:
Todo commit sin cumplir las anotaciones o descripcion es **revertido** y su creador tendra un llamado de **atencion**.

# Ayuda o Informacion adicional
Para ayuda o informacion adicional ingresa en el [servidor de discord](https://discord.gg/mdaFnpD)!
